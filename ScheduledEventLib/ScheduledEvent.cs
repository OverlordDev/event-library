﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/*
 Scheduled Event General Structure

    1. Instantiate a ScheduledEvent class object
    2. Update the object in the game update function
    3. Create an enum with all the event types 
    4. Implement a switch(GetNextEvent()) that will handle the next event
    5. Under each case there can be added multiple actions
    6. Switch default is always null 

    Example:

    

    enum EventTypes
    {
        EVENT_TYPE_A,
        EVENT_TYPE_B,
        EVENT_TYPE_C

    }

    ScheduledEvent events = new ScheduledEvent();

    events.ScheduleEvent(EVENT_TYPE_A, 1.5, 0);
    events.ScheduleEvent(EVENT_TYPE_B, 2.5, 0);
    events.ScheduleEvent(EVENT_TYPE_C, 5.5, 0);


    void Update()
    {   
        events.Update(Time.deltaTime); // or any time interval used by the game


        while(events.GetNextEvent())
        {
                switch(events.ExecuteEvent())
                {
                case EVENT_TYPE_A:
                    //something
                    break;

                case EVENT_TYPE_B:
                    //something
                    break;

                case EVENT_TYPE_C:
                    //something
                    break;

                default:
                    //Possible error, use it for debugging
                    break;
            
               }
        }
        
    }

  */


namespace ScheduledEventLib
{  
    public class ScheduledEvent
    {
        public ScheduledEvent()
        {
            //constructor
            eventList = new Dictionary<int, Event>();
            _phase = 0;
        }

        //Update function
        public void Update(int t_time) // time in S
        {
            foreach (KeyValuePair<int, Event> entry in eventList)
            {
                if ((entry.Value._phase == _phase) && (entry.Value.GetState() == true))
                {
                    //check for time intervals
                    if (entry.Value._time - t_time <= 0)
                    {
                        entry.Value.SetEventTime(0);
                        entry.Value.SetState(false);
                    }
                    else
                        entry.Value.SetEventTime(entry.Value._time - t_time);
                }
            }
        }

        // Schedule Event 
        public void ScheduleEvent(int eventEntry, int scheduledTime, int phase)
        {
            Event v = new Event(eventEntry, scheduledTime, phase);
            if (scheduledTime == 0)
                v.SetState(false);
            eventList.Add(eventEntry, v);
            return;
        }

        //Cancel event
        public void CancelAllEvents()
        {
            eventList.Clear();
        }
        public void CancelEventPhase(int phase)
        {
            foreach (KeyValuePair<int, Event> valuePair in eventList)
            {
                if (valuePair.Value._phase == phase)
                    eventList.Remove(valuePair.Key);
            }
        }
        public void CancelEvent(int entry)
        {
            foreach (KeyValuePair<int, Event> valuePair in eventList)
            {
                if (valuePair.Value._eventEntry == entry && valuePair.Value.GetState())
                    eventList.Remove(valuePair.Key);
            }
        }

        // Execution
        public int ExecuteEvent()
        {
            foreach (KeyValuePair<int, Event> valuePair in eventList)
            {
                if (ValidateEventPhase(valuePair.Value._phase, _phase) && valuePair.Value.GetState() == false)
                {
                    var v = valuePair.Value._eventEntry;
                    eventList.Remove(valuePair.Key);
                    return v;
                }
            }
            return 0;
        }
        public int GetNextEvent()
        {
            foreach (KeyValuePair<int, Event> valuePair in eventList)
            {
                if (ValidateEventPhase(valuePair.Value._phase, _phase) && valuePair.Value.GetState() == false)
                {
                    return valuePair.Value._eventEntry;
                }

            }
            return 0;
        }

        //Timing methods
        public int TimeUntilNextEvent()
        {
            bool firstCheck = true;
            var min = 0;
            foreach (KeyValuePair<int, Event> valuePair in eventList)
            {
                if (ValidateEventPhase(_phase, valuePair.Value._phase) && firstCheck)
                {
                    min = valuePair.Value._time;
                    firstCheck = false;
                    continue;
                }

                if (ValidateEventPhase(_phase, valuePair.Value._phase) && !firstCheck)
                {
                    min = valuePair.Value._time < min ? valuePair.Value._time : min;
                }
            }
            return min;
        }
        public int TimeUntilNextEvent(int entry)
        {
            bool firstCheck = true;
            var min = 0;
            foreach (KeyValuePair<int, Event> valuePair in eventList)
            {
                if (ValidateEventPhase(_phase, valuePair.Value._phase) && valuePair.Value._eventEntry == entry && firstCheck)
                {
                    min = valuePair.Value._time;
                    firstCheck = false;
                    continue;
                }

                if (ValidateEventPhase(_phase, valuePair.Value._phase) && valuePair.Value
._eventEntry == entry && !firstCheck)
                {
                    min = valuePair.Value._time < min ? valuePair.Value._time : min;
                }
            }
            return min;
        }

        //Delay methods
        public void DelayAllEvents(int delay)
        {
            foreach (KeyValuePair<int,Event> valuePair in eventList)
            {
                if (valuePair.Value.GetState())
                    valuePair.Value.SetEventTime(valuePair.Value._time + delay);
            }
        }
        public void DelayEvent(int eventEntry, int delay)
        {
            if (eventList[eventEntry].GetState())
                eventList[eventEntry].SetEventTime(eventList[eventEntry]._time + delay);   
        }

        //Utility
        public bool ValidateEventPhase(int basePhase, int eventPhase)
        {
            return (basePhase & eventPhase) == 0 ? false : true;
        }
        public int GetPhase() { return _phase; }
        public void SetPhase(int phase, bool reset = true)
        {
            if (reset)
            {
                foreach (KeyValuePair<int, Event> valuePair in eventList)
                {
                    if (ValidateEventPhase(_phase, valuePair.Value._phase))
                        valuePair.Value.SetEventTime(valuePair.Value._scheduledTime);
                }
            }
            _phase = phase;
        }

        private Dictionary<int, Event> eventList; // main list with events
        private int _phase; // phase of the event object, not directly related to the phase of events
    }
    
    public class Event
    {
        public int _eventEntry;
        public int _scheduledTime;
        public int _phase;
        public int _time; // time in MS
        public bool _scheduleUpdate;

        public Event(int eventEntry, int scheduledTime, int phase, bool state = true)
        {
            // class constructor
            _eventEntry = eventEntry;
            _scheduledTime = scheduledTime;
            _phase = phase;
            _time = scheduledTime;
            _scheduleUpdate = state;
        }

        // Utility 
        public void SetEventTime(int time) { _time = time; }
        public void SetState(bool state) { _scheduleUpdate = state; }
        public bool GetState() { return _scheduleUpdate; }
       
    }  
}



